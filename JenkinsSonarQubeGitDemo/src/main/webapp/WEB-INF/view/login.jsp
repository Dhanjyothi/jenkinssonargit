<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
 
<head>
  <script type="text/javascript" src="/js/jquery-3.1.0.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"></link>
    <link href="/css/layout.css" rel="stylesheet" type="text/css"></link>
   
</head>

<body>
    <div class="container">
        <form:form class="form-horizontal" action="addCustomer" modelAttribute="login">
            <fieldset>

                <!-- Form Name -->
                <legend>User Login</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="username">Username</label>
                    <div class="col-md-4">
                        <form:input path="username" name="username" type="text" placeholder="Username" class="form-control input-md" required="" />

                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="password">Password</label>
                    <div class="col-md-4">
                        <form:input path="password" name="password" type="password" placeholder="Password" class="form-control input-md" required="" />

                    </div>
                </div>

               
                

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-primary btn-login">Login</button>
                    </div>
                </div>
                </fieldset>
        </form:form>
                
</div>
            
      
 
</body>

</html>