package com.hcl.webinar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.webinar.model.Customer;
import com.hcl.webinar.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@RequestMapping(value = "/getAllCustomers", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Customer> getAllCustomers(Model model) {

		List<Customer> listOfCustomers = customerService.getAllCustomers();
		model.addAttribute("customer", new Customer());
		model.addAttribute("listOfCustomers", listOfCustomers);
		return listOfCustomers ;
	}

	/*@RequestMapping(value = "/", method = RequestMethod.GET, headers = "Accept=application/json")
	public ModelAndView goToHomePage() {
		return new ModelAndView("redirect:/getAllCustomers");
	}
	
	@RequestMapping(value = "/getCustomer/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Customer getCustomerById(@PathVariable int id) {
		Customer customer=customerService.getCustomer(id);
		System.out.println("Email"+customer.getEmail());
		return customer;
	}*/
	@RequestMapping(value = "/addCustomer")
	public ModelAndView addCustomer(@ModelAttribute("customer") Customer customer) {	

		return new ModelAndView("addCustomer");
	}

	@RequestMapping(value = "/saveCustomer")
	public String saveCustomer(@ModelAttribute("customer") Customer customer) {	
		if(customer.getId()==0)
		{
			
			
			customerService.addCustomer(customer);
			
		}
		return "Saved Successfully";
			
		
	}

/*	@RequestMapping(value = "/updateCustomer/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ModelAndView updateCustomer(@PathVariable("id") int id,Model model) {
		model.addAttribute("customer", this.customerService.getCustomer(id));
		model.addAttribute("listOfCustomers", this.customerService.getAllCustomers());
		return new ModelAndView("customerDetails");
	}

	@RequestMapping(value = "/deleteCustomer/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ModelAndView deleteCustomer(@PathVariable("id") int id) {
		customerService.deleteCustomer(id);
		return new ModelAndView("redirect:/getAllCustomers");

	}	*/
}
